import React, { PureComponent } from "react";
import "./Card.css";
import CardBanner from "./CardBanner.jsx";
import CardContent from "./CardContent.jsx";

class CardInner extends PureComponent {
  state = {};
  render() {
    return (
      <div className="cardInner">
        <CardBanner />
        <CardContent />
      </div>
    );
  }
}

export default CardInner;
