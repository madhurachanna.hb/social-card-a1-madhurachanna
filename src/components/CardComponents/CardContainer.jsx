import React, { PureComponent } from "react";
import "./Card.css";
import CardInner from "./CardInner.jsx";

class CardContainer extends PureComponent {
  state = {};
  render() {
    return (
      <div className="cardContainer">
        <CardInner />
      </div>
    );
  }
}

export default CardContainer;
