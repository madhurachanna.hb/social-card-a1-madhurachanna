import React, { PureComponent } from "react";
import "./Card.css";

class CardContent extends PureComponent {
  state = {};
  render() {
    return (
      <div className="cardContent">
        <h7>Get started with React</h7>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tempore enim
          voluptatibus debitis culpa corrupti?
        </p>
      </div>
    );
  }
}

export default CardContent;
