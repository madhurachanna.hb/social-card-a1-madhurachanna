import React, { PureComponent } from "react";
import "./Card.css";

class CardBanner extends PureComponent {
  state = {};
  render() {
    return (
      <img
        id="bannerImg"
        src="https://tk-assets.lambdaschool.com/fcd75197-7d12-46ec-bc9e-4130f34822fa_reactbackground.png"
        alt=""
      />
    );
  }
}

export default CardBanner;
