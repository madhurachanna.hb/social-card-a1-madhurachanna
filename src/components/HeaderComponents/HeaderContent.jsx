import React, { PureComponent } from "react";
import "./Header.css";
import HeaderTitle from "./HeaderTitle";

class HeaderContent extends PureComponent {
  state = {};
  render() {
    return (
      <div className="headerContent">
        <HeaderTitle />
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima autem
          molestiae minus vero, molestias a neque repellendus rerum? Possimus
          laboriosam a velit cum, odit reiciendis id architecto aperiam eveniet
          alias!
        </p>
      </div>
    );
  }
}

export default HeaderContent;
