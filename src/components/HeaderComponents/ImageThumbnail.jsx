import React, { PureComponent } from "react";
import "./Header.css";

class ImageThumb extends PureComponent {
  state = {};
  render() {
    return (
      <img
        src="https://tk-assets.lambdaschool.com/1c1b7262-cf23-4a9f-90b6-da0d3c74a5c6_lambdacrest.png"
        alt="Logo"
        className="logo"
      />
    );
  }
}

export default ImageThumb;
