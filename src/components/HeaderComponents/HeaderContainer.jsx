import React, { PureComponent } from "react";
import "./Header.css";
import ImageThumbnail from "./ImageThumbnail.jsx";
import HeaderContent from "./HeaderContent";

class HeaderContainer extends PureComponent {
  state = {};
  render() {
    return (
      <div className="headerContainer">
        <ImageThumbnail />
        <HeaderContent />
      </div>
    );
  }
}

export default HeaderContainer;
