import React, { PureComponent } from "react";
import "./Header.css";

class HeaderTitle extends PureComponent {
  state = {};
  render() {
    return <div className="headerTitle">Lambda School</div>;
  }
}

export default HeaderTitle;
