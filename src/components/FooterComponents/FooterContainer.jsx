import React, { PureComponent } from "react";
import "./Footer.css";

class FooterContainer extends PureComponent {
  state = {
    count: 0
  };

  handleLike = () => {
    console.log("img clicked");
    this.setState({
      count: this.state.count + 1
    });
    console.log(this.state.count);
  };
  render() {
    return (
      <div className="footerContainer">
        <div onClick={this.handleLike} className="likeButtonDiv">
          <img src={require("../Images/heart.png")} alt="" />
          {/* L */}
        </div>

        <input type="text" id="input1" value={this.state.count} />
      </div>
    );
  }
}

export default FooterContainer;
