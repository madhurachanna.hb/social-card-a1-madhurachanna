import React, { PureComponent } from "react";
import "./Footer.css";
import FooterContainer from "./FooterContainer.jsx";

class Footer extends PureComponent {
  state = {};
  render() {
    return (
      <div className="footer">
        <FooterContainer />
      </div>
    );
  }
}

export default Footer;
