import React, { PureComponent } from "react";
import "./App.css";
import HeaderContainer from "./components/HeaderComponents/HeaderContainer";
import Card from "./components/CardComponents/CardContainer.jsx";
import Footer from "./components/FooterComponents/Footer.jsx";

class App extends PureComponent {
  state = {};
  render() {
    return (
      <div className="bodyBox">
        <HeaderContainer />
        <Card />
        <Footer />
      </div>
    );
  }
}

export default App;
